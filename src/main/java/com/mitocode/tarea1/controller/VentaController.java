package com.mitocode.tarea1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.tarea1.exception.ModeloNotFoundException;
import com.mitocode.tarea1.model.Venta;
import com.mitocode.tarea1.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private IVentaService service;
	
	
	@GetMapping
	public ResponseEntity<List<Venta>> listar(){
		List<Venta> lista = service.listar();
		return new ResponseEntity<List<Venta>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Venta> listarById(@PathVariable Integer id){
		Venta ven = service.listarById(id);
		if (ven.getIdVenta() == null) {
			throw new ModeloNotFoundException("ID VENTA NO EXISTE EN BD: " + id);
		}
		return new ResponseEntity<Venta>(ven, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Venta> registrar(@Valid @RequestBody Venta obj){
		Venta ven = service.registrar(obj);
		return new ResponseEntity<Venta>(ven, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Venta> modificar(@Valid @RequestBody Venta obj){
		Venta ven = service.listarById(obj.getIdVenta());
		if (ven.getIdVenta() == null) {
			throw new ModeloNotFoundException("ID VENTA A MODIFICAR NO EXISTE EN BD: " + obj.getIdVenta());
		}
		ven = service.modificar(obj);
		return new ResponseEntity<Venta>(ven, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Integer id){
		Venta ven = service.listarById(id);
		if (ven.getIdVenta() == null) {
			throw new ModeloNotFoundException("ID VENTA A ELIMINAR NO EXISTE EN BD: " + id);
		}
		service.delete(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
