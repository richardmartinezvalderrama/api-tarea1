package com.mitocode.tarea1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.tarea1.exception.ModeloNotFoundException;
import com.mitocode.tarea1.model.Persona;
import com.mitocode.tarea1.service.IPersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {

	@Autowired
	private IPersonaService service;
	
	
	@GetMapping
	public ResponseEntity<List<Persona>> listar(){
		List<Persona> lista = service.listar();
		return new ResponseEntity<List<Persona>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Persona> listarById(@PathVariable Integer id){
		Persona per = service.listarById(id);
		if (per.getIdPersona() == null) {
			throw new ModeloNotFoundException("ID PERSONA NO EXISTE EN BD: " + id);
		}
		return new ResponseEntity<Persona>(per, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Persona> registrar(@Valid @RequestBody Persona obj){
		Persona per = service.registrar(obj);
		return new ResponseEntity<Persona>(per, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Persona> modificar(@Valid @RequestBody Persona obj){
		Persona per = service.listarById(obj.getIdPersona());
		if (per.getIdPersona() == null) {
			throw new ModeloNotFoundException("ID PERSONA A MODIFICAR NO EXISTE EN BD: " + obj.getIdPersona());
		}
		per = service.modificar(obj);
		return new ResponseEntity<Persona>(per, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Integer id){
		Persona per = service.listarById(id);
		if (per.getIdPersona() == null) {
			throw new ModeloNotFoundException("ID PERSONA A ELIMINAR NO EXISTE EN BD: " + id);
		}
		service.delete(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
