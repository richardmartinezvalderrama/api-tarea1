package com.mitocode.tarea1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.tarea1.exception.ModeloNotFoundException;
import com.mitocode.tarea1.model.Producto;
import com.mitocode.tarea1.service.IProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private IProductoService service;
	
	
	@GetMapping
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> lista = service.listar();
		return new ResponseEntity<List<Producto>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Producto> listarById(@PathVariable Integer id){
		Producto prod = service.listarById(id);
		if (prod.getIdProducto() == null) {
			throw new ModeloNotFoundException("ID PRODUCTO NO EXISTE EN BD: " + id);
		}
		return new ResponseEntity<Producto>(prod, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Producto> registrar(@Valid @RequestBody Producto obj){
		Producto prod = service.registrar(obj);
		return new ResponseEntity<Producto>(prod, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Producto> modificar(@Valid @RequestBody Producto obj){
		Producto prod = service.listarById(obj.getIdProducto());
		if (prod.getIdProducto() == null) {
			throw new ModeloNotFoundException("ID PRODUCTO A MODIFICAR NO EXISTE EN BD: " + obj.getIdProducto());
		}
		prod = service.modificar(obj);
		return new ResponseEntity<Producto>(prod, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Integer id){
		Producto prod = service.listarById(id);
		if (prod.getIdProducto() == null) {
			throw new ModeloNotFoundException("ID PRODUCTO A ELIMINAR NO EXISTE EN BD: " + id);
		}
		service.delete(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
