package com.mitocode.tarea1.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mitocode.tarea1.model.Venta;


public interface IVentaRepo extends JpaRepository<Venta, Integer> {

}
