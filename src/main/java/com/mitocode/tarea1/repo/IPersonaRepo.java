package com.mitocode.tarea1.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mitocode.tarea1.model.Persona;


public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
