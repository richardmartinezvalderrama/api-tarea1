package com.mitocode.tarea1.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mitocode.tarea1.model.Producto;


public interface IProductoRepo extends JpaRepository<Producto, Integer> {

}
