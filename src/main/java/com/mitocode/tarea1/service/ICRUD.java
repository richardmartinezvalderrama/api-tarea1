package com.mitocode.tarea1.service;

import java.util.List;

public interface ICRUD<T> {

	List<T> listar();
	
	T listarById(Integer id);
	
	T registrar(T obj);
	
	T modificar(T obj);
	
	boolean delete(Integer id);
	
}
