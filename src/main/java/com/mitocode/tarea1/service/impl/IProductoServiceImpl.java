package com.mitocode.tarea1.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.tarea1.model.Producto;
import com.mitocode.tarea1.repo.IProductoRepo;
import com.mitocode.tarea1.service.IProductoService;

@Service
public class IProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoRepo repo;

	@Override
	public List<Producto> listar() {
		return repo.findAll();
	}

	@Override
	public Producto listarById(Integer id) {
		Optional<Producto> opt = repo.findById(id);
		return opt.isPresent() ? opt.get() : new Producto();
	}

	@Override
	public Producto registrar(Producto obj) {
		return repo.save(obj);
	}

	@Override
	public Producto modificar(Producto obj) {
		return repo.save(obj);
	}

	@Override
	public boolean delete(Integer id) {
		repo.deleteById(id);
		return true;
	}
	
	
}
