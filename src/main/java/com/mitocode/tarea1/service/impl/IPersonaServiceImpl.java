package com.mitocode.tarea1.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.tarea1.model.Persona;
import com.mitocode.tarea1.repo.IPersonaRepo;
import com.mitocode.tarea1.service.IPersonaService;

@Service
public class IPersonaServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaRepo repo;

	@Override
	public List<Persona> listar() {
		return repo.findAll();
	}

	@Override
	public Persona listarById(Integer id) {
		Optional<Persona> opt = repo.findById(id);
		return opt.isPresent() ? opt.get() : new Persona();
	}

	@Override
	public Persona registrar(Persona obj) {
		return repo.save(obj);
	}

	@Override
	public Persona modificar(Persona obj) {
		return repo.save(obj);
	}

	@Override
	public boolean delete(Integer id) {
		repo.deleteById(id);
		return true;
	}

	

}
