package com.mitocode.tarea1.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.tarea1.model.Venta;
import com.mitocode.tarea1.repo.IVentaRepo;
import com.mitocode.tarea1.service.IVentaService;

@Service
public class IVentaServiceImpl implements IVentaService {
	
	@Autowired
	private IVentaRepo repo;
	
	@Override
	public List<Venta> listar() {
		return repo.findAll();
	}

	@Override
	public Venta listarById(Integer id) {
		Optional<Venta> opt = repo.findById(id);
		return opt.isPresent() ? opt.get() : new Venta();
	}

	@Override
	public Venta registrar(Venta obj) {
		obj.getLstDetallesVentas().forEach(det -> {
			det.setVenta(obj);
		});
		return repo.save(obj);
	}

	@Override
	public Venta modificar(Venta obj) {
		return repo.save(obj);
	}

	@Override
	public boolean delete(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
