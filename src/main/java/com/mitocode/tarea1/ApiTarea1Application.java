package com.mitocode.tarea1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTarea1Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiTarea1Application.class, args);
	}

}
